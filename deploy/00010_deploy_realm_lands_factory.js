module.exports = async ({ getNamedAccounts, deployments }) => {
  const { deploy, log } = deployments;
  const { deployer } = await getNamedAccounts();

  let implementation = await deploy('RealmLandsFactory', {
    from: deployer,
    log: true,
    args: [],
  });

  await deploy('RealmLands', {
    from: deployer,
    log: true,
    args: [
      deployer
    ],
  });

};

module.exports.tags = ['realm_lands_factory'];
