const { ethers } = require('hardhat');

module.exports = async ({ getNamedAccounts, deployments }) => {
  const { deploy, log } = deployments;
  const { deployer } = await getNamedAccounts();

  const realm = await ethers.getContract('Realm');
  const realmLandsFactory = await ethers.getContract('RealmLandsFactory');
  const distributionManager = await ethers.getContract('DistributionManager');
  const protocolParameters = await ethers.getContract('ProtocolParameters');

  // this contract is upgradeable through uups (EIP-1822)
  await deploy('RealmCreator', {
    from: deployer,
    log: true,
    args: [],
    proxy: {
      proxyContract: 'UUPSProxy',
      execute: {
        init: {
          methodName: 'initialize',
          args: [
            [
              realm.address,
              distributionManager.address,
              protocolParameters.address,
              realmLandsFactory.address
            ]
          ],
        },
      },
    }
  });
};

module.exports.tags = ['creator'];
module.exports.dependencies = ['distribution_manager', 'realm', 'realm_lands_factory', 'protocol_parameters'];
