module.exports = async ({ getNamedAccounts, deployments }) => {
  const { deploy, log } = deployments;
  const { deployer } = await getNamedAccounts();

  let implementation = await deploy('ERC20Mock', {
    from: deployer,
    log: true,
    args: [],
  });
  
};

module.exports.tags = ['erc20mock'];
