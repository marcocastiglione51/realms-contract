module.exports = async ({ getNamedAccounts, deployments }) => {
  const { deploy, log } = deployments;
  const { deployer } = await getNamedAccounts();

  let implementation = await deploy('Realm', {
    from: deployer,
    log: true,
    args: [],
  });

};

module.exports.tags = ['realm'];
