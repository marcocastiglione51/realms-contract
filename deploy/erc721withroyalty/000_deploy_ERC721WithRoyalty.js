module.exports = async ({ getNamedAccounts, deployments }) => {
  const { deploy } = deployments;
  const { deployer } = await getNamedAccounts();

  console.log(deployer, 'deployer');

  // this contract is upgradeable through uups (EIP-1822)
  const RoyaltyFactory = await deploy('ERC721WithRoyalty', {
    from: deployer,
    log: true,
    args: ['test', 'test']
  });

};

module.exports.tags = ['erc721'];
