const { expect } = require('chai');
const { ethers, network } = require('hardhat');
const { getEventArgs } = require('../test/helpers/events');


async function asyncCall() {

    /* address */
    [owner] = await ethers.getSigners();

    // realm factory address
    const realmCreatorAdress = '';

    // NFT to attach to realm
    const nftAddress = '';
    const nftId = 0;

    const realmCreator = await ethers.getContractAt('RealmCreator', realmCreatorAdress);

    const tx = await realmCreator.createRealm(
        [
            1000, // tax rate
            2000, // creator share
            5100, //voting consensus
            nftId // nft to attach ID
        ], 
        nftAddress, // nft to attach address
        'CONTRACT_URI', 
        'REALM_NAME', 
        'REALM_SYMBOL'
    );

    await expect(tx).to.emit(realmCreator, 'RealmCreated');
    const ARGS = await getEventArgs(tx, 'RealmCreated', realmCreator);
    realmAddress = ARGS.realmAddress;

    const realm = await ethers.getContractAt('Realm', realmAddress);

    // Parameters for new asset
    const fundingTokenAddress = '';
    const extensionId = 0;
    const numberOfEditions = 0;
    const salePrice = 0;
    const rentalPrice = 0;
    const maximumRentalTime = 0;

    const uploadTx = await realm.uploadAsset(
        'TOKEN_URI',
        fundingTokenAddress,
        [
            extensionId,
            numberOfEditions,
            salePrice,
            rentalPrice,
            maximumRentalTime
        ]
    );

    await expect(uploadTx).to.emit(realm, 'AssetUploaded');
    const uploadArgs = await getEventArgs(uploadTx, 'AssetUploaded', realm);
    
    const assetId = uploadArgs.assetId;
    const asset = await realm.assets(assetId);

    const realmFundingTokenAddress = asset.fundingToken;
    const fundingToken = await ethers.getContractAt('ERC20', realmFundingTokenAddress);
    
    // How to buy/rent NFT with lazy minting

    // Buy

    const realmSalePrice = asset.salePrice;
    await fundingToken.approve(realmSalePrice);
    const claimTx = await realm.claimAsset(assetId);

    expect(claimTx).to.emit(realm, 'AssetClaimed');
    const claimArgs = await getEventArgs(claimTx, 'AssetClaimed', realm);
    const tokenId = claimArgs.tokenId;



    // Rent

    const realmRentalPrice = asset.rentalPrice;
    const rentalTime = 1;

    const totalRentalPrice = realmRentalPrice * rentalTime;

    await fundingToken.approve(totalRentalPrice);

    const rentTx = await realm.rentAsset(
        assetId,
        rentalTime 
    );

    expect(rentTx).to.emit(realm, 'AssetClaimed');
    const rentArgs = await getEventArgs(rentTx, 'AssetClaimed', realm);
    const rentTokenId = claimArgs.tokenId;



}

asyncCall();
