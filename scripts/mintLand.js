const { expect } = require('chai');
const { ethers, network } = require('hardhat');

const parseAmount = (amount) => ethers.utils.parseEther(amount);

async function asyncCall() {
    [owner] = await ethers.getSigners();

    const provider = ethers.providers.Provider;

    const realmCreatorAddress = '';
    const nftAddress = '';
    const nftId = 0;

    const realmCreator = await ethers.getContractAt('RealmCreator', realmCreatorAddress);

    const tx = await realmCreator.createRealm(
        [
            1000, // tax rate
            2000, // creator share
            5100, //voting consensus
            nftId // nft to attach ID
        ], 
        nftAddress, // nft to attach address
        'CONTRACT_URI', 
        'REALM_NAME', 
        'REALM_SYMBOL'
    );

    await expect(tx).to.emit(realmCreator, 'RealmCreated');
    const ARGS = await getEventArgs(tx, 'RealmCreated', realmCreator);
    realmAddress = ARGS.realmAddress;
    extensionId = 0; // DEFAULT 0 FOR NEW REALMS

    console.log('REALM ADDRESS', realmAddress);

    const realm = await ethers.getContractAt('Realm', realmAddress);

    // To mint land (NOT BATCH)
    await realm.mintLand(
        0, // extension ID
        owner.address, // mint to...
        '' // token URI
    );

    // To mint land (BATCH)
    await realm.mintLandBatch(
        0, 
        owner.address,
        10,
        ['URI1','URI2','URI3','URI4','URI5','URI6','URI7','URI8','URI9','URI10']
    );

}

asyncCall();
