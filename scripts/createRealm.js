const { expect } = require('chai');
const { ethers, network } = require('hardhat');

async function asyncCall() {

    /* address */
    [owner] = await ethers.getSigners();

    const realmCreatorAdress = '';
    const nftAddress = '';
    const nftId = 0;

    const realmCreator = await ethers.getContractAt('RealmCreator', realmCreatorAdress);

    const tx = await realmCreator.createRealm(
        [
            1000, // tax rate
            2000, // creator share
            5100, //voting consensus
            nftId // nft to attach ID
        ], 
        nftAddress, // nft to attach address
        'CONTRACT_URI', 
        'REALM_NAME', 
        'REALM_SYMBOL'
    );

    await expect(tx).to.emit(realmCreator, 'RealmCreated');
    const ARGS = await getEventArgs(tx, 'RealmCreated', realmCreator);
    realmAddress = ARGS.realmAddress;

    console.log('REALM ADDRESS', realmAddress);

}

asyncCall();
