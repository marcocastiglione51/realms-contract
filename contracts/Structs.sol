// SPDX-License-Identifier: MIT

pragma solidity 0.8.4;

struct ProposalData{
    address tokenAddress;
    uint256 tokenId;
    address owner;
}

struct Asset {
    string  tokenURI;
    address fundingToken;
    uint256 extensionId;
    uint256 currentId;
    uint256 numberOfEditions;
    uint256 mintedEditions;
    uint256 salePrice;
    uint256 rentalPrice;
    uint256 maxRentalTime;
}
