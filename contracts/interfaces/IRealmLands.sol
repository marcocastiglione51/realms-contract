// SPDX-License-Identifier: MIT

pragma solidity 0.8.4;

import "@openzeppelin/contracts/token/ERC721/IERC721.sol";

interface IRealmLands is IERC721 {
    function initialize() external;

    function mint(address to, string memory uri_) external returns (uint256);

    function mintBatch(address to, uint256 qtyToMint, string[] memory uris) external returns (uint256);

    function getCurrentId() external returns(uint256 tokenId_);

    function incrementId(uint256 num) external;

    function mintWithId(address to, uint256 tokenId_, string memory uri_, uint256 rentalTime_) external;
}
