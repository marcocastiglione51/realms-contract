// SPDX-License-Identifier: MIT
pragma solidity ^0.8.2;


interface IProtocolParameters {

    function depositFee() external view returns(uint256);

    function protocolFee() external view returns(uint256);

    function feeCollector() external view returns(address);

    function setDepositFee(uint256 depositFee_) external;

    function setProtocolFee(uint256 protocolFee_) external;

    function setSellerFee(address feeCollector_) external;
}