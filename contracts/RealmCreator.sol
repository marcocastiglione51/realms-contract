// SPDX-License-Identifier: MIT

pragma solidity 0.8.4;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";
import "@openzeppelin/contracts/proxy/Clones.sol";
import "./Realm.sol";
import "./RealmLands.sol";
import "./Voting.sol";
import "hardhat/console.sol";

contract RealmCreator is OwnableUpgradeable, UUPSUpgradeable {

    event RealmCreated(
        uint256 votingTime,
        uint256 taxRate,
        uint256 creatorShare,
        address owner,
        address upgrader,
        address nftAddress,
        uint256 nftId,
        address realmAddress,
        address distributionManager
    );

    address private _distributionManager;
    address private _protocolParameters;
    address private _realm;
    address private _realmLandsAddress;
    address private _marketplaceFactory;

    constructor() {}

    function initialize(
        address[4] memory addressValues
        // address realm_,
        // address distributionManager_,
        // address protocolParameters_,
        // address realmLandsAddress_,
        // address marketplaceFactory_
    ) external initializer {
        __Ownable_init();
        __UUPSUpgradeable_init();

        _realm = addressValues[0];
        _distributionManager = addressValues[1];
        _protocolParameters = addressValues[2];
        _realmLandsAddress = addressValues[3];
        // _marketplaceFactory = addressValues[4];

    }

    function createRealm(
        uint256[] memory uintValues,
        //uint256 taxRate,
        //uint256 creatorShare,
        //uint256 votingConsensus,
        //uint256 nftToAttachId,
        address nftToAttachAddress,
        string memory contractURI_,
        string memory name_,
        string memory symbol_
    ) external returns (address realmAddress) {

        address ownerOf = IERC721(nftToAttachAddress).ownerOf(uintValues[3]);
        require(ownerOf == msg.sender, "You are not the owner of the ERC721!");

        realmAddress = Clones.clone(_realm);

        // default value
        uint256 votingTime = 604800;

        Realm(realmAddress).initialize(
            [
                msg.sender, 
                owner(),
                _protocolParameters,
                _distributionManager,
                nftToAttachAddress,
                _realmLandsAddress
            ],
            uintValues,
            votingTime,
            [
                contractURI_,
                name_,
                symbol_
            ]
        );

        emit RealmCreated(
            votingTime,
            uintValues[0],
            uintValues[1],
            msg.sender,
            owner(),
            nftToAttachAddress,
            uintValues[3],
            realmAddress,
            _distributionManager
        );
    }

    function setRealmAddress(address realm_) external onlyOwner {
        _realm = realm_;
    }

    function _authorizeUpgrade(address newImplementation) internal override onlyOwner {}

}
