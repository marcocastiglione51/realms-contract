// SPDX-License-Identifier: MIT

pragma solidity 0.8.4;

import "./RealmLands.sol";
import "hardhat/console.sol";

contract RealmLandsFactory {

    constructor() {}

    function newRealmLands() external returns (address realmLands) {        
        realmLands = address(new RealmLands(msg.sender));
    }


}
