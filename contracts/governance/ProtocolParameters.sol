// SPDX-License-Identifier: MIT
pragma solidity ^0.8.2;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";

contract ProtocolParameters is OwnableUpgradeable, UUPSUpgradeable {

    /// @notice value in ETH to keep when adding extension
    uint256 public depositFee;

    /// @notice percentage of the depositFee that will be charged by protocol
    uint256 public protocolFee;

    /// @notice address of the fee collector
    address public feeCollector;

    event DepositFeeUpdated(uint256 from, uint256 to);
    event ProtocolFeeUpdated(uint256 from, uint256 to);
    event FeeCollectorUpdated(address from, address to);

    constructor() {}

    function initialize(
        uint256 depositFee_,
        uint256 protocolFee_,
        address feeCollector_
    ) external initializer {
        __Ownable_init();
        __UUPSUpgradeable_init();

        require(depositFee_ > 0, "Invalid deposit fee");
        require(protocolFee_ > 0, "Invalid protocol fee");
        require(feeCollector_ != address(0), "Invalid fee collector");

        depositFee = depositFee_;
        protocolFee = protocolFee_;
        feeCollector = feeCollector_;
    }

    function setDepositFee(uint256 depositFee_) external onlyOwner {
        require(depositFee_ > 0, "Invalid buyer fee");
        emit DepositFeeUpdated(depositFee, depositFee_);
        depositFee = depositFee_;
    }

    function setProtocolFee(uint256 protocolFee_) external onlyOwner {
        require(protocolFee_ > 0, "Invalid buyer fee");
        emit ProtocolFeeUpdated(protocolFee, protocolFee_);
        protocolFee = protocolFee_;
    }

    function setSellerFee(address feeCollector_) external onlyOwner {
        require(feeCollector_ != address(0), "Invalid fee collector");
        emit FeeCollectorUpdated(feeCollector, feeCollector_);
        feeCollector = feeCollector_;
    }

    function _authorizeUpgrade(address newImplementation) internal override onlyOwner {}
}