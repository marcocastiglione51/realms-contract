// SPDX-License-Identifier: MIT

pragma solidity 0.8.4;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract RealmLands is ERC721, Ownable {

    uint256 private tokenId;

    // Token name
    string private _proxiedName;

    // Token symbol
    string private _proxiedSymbol;

    mapping(uint256 => string) private _tokenToUri;

    mapping(uint256 => uint256) public rentedTokensExpiration;

    constructor(
        address owner_
    ) ERC721("Realm Lands", "RLANDS") {
        _transferOwnership(owner_);
    }

    function getCurrentId() external view returns(uint256 tokenId_) {
        tokenId_ = tokenId;
    }

    function incrementId(uint256 num) external onlyOwner {
        tokenId += num;
    }

    function mintWithId(
        address to,
        uint256 tokenId_,
        string memory uri_,
        uint256 rentalTime
    ) external onlyOwner {
        _safeMint(to, tokenId_);
        _tokenToUri[tokenId_] = uri_;

        if (rentalTime > 0) {
            rentedTokensExpiration[tokenId_] = block.timestamp + rentalTime;
        } 
    }

    function isRental(uint256 tokenId_) public view returns(bool) {
        require(
            _exists(tokenId_),
            "ERC721Metadata: URI query for nonexistent token"
        );
        return rentedTokensExpiration[tokenId_] > 0;
    }

    function isRentalExpired(uint256 tokenId_) public view returns(bool) {
        return rentedTokensExpiration[tokenId_] > 0 && rentedTokensExpiration[tokenId_] <= block.timestamp;
    }

    function mint(
        address to, 
        string memory uri_
    ) external onlyOwner returns (uint256 tokenId_) {
        tokenId_ = _mint(to, uri_);
    }

    function _mint(
        address to, 
        string memory uri_
    ) internal returns (uint256) {
        uint256 mintedTokenId = tokenId;
        tokenId += 1;
        _safeMint(to, mintedTokenId);

        _tokenToUri[mintedTokenId] = uri_;

        return mintedTokenId;
    }

    function mintBatch(
        address to,
        uint256 qtyToMint,
        string[] memory uris
    ) external onlyOwner returns(uint256 initialId) {

        require(qtyToMint == uris.length, "uris must be same length as quantity to mint");

        for (uint256 i = 0; i < qtyToMint; i++) {
            if (initialId == 0) {
                initialId = _mint(to, uris[i]);
            } else {
                _mint(to, uris[i]);
            }
        }
    }

    function name() public view virtual override returns (string memory) {
        return _proxiedName;
    }

    function symbol() public view virtual override returns (string memory) {
        return _proxiedSymbol;
    }

    function tokenURI(uint256 tokenId_)
        public
        view
        override
        returns (string memory)
    {
        require(
            _exists(tokenId_),
            "ERC721Metadata: URI query for nonexistent token"
        );

        require(
            !isRentalExpired(tokenId_),
            "Token rental has expired!"
        );

        return _tokenToUri[tokenId_];
        
    }

    function transferFrom(
        address from,
        address to,
        uint256 tokenId_
    ) public virtual override {
        //solhint-disable-next-line max-line-length
        require(
            _isApprovedOrOwner(
                _msgSender(), 
                tokenId_
            ), 
            "ERC721: transfer caller is not owner nor approved"
        );

        require(
            !isRentalExpired(tokenId_),
            "Token rental has expired!"
        );

        _transfer(from, to, tokenId_);
    }


}
