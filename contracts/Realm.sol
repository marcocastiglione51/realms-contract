// SPDX-License-Identifier: MIT

pragma solidity 0.8.4;

import "@openzeppelin/contracts/token/ERC721/extensions/IERC721Metadata.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC721/ERC721Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/AccessControlUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";
import "@openzeppelin/contracts/proxy/Clones.sol";
import "./interfaces/IProtocolParameters.sol";
import "./Voting.sol";
import "./Structs.sol";
import "./interfaces/IRealmLands.sol";
import "./interfaces/IRealmLandsFactory.sol";
import "hardhat/console.sol";

contract Realm is ERC721Upgradeable, Voting, AccessControlUpgradeable, UUPSUpgradeable {

    bytes32 public constant REALM_OWNER = keccak256("REALM_OWNER");

    enum ProposalTypes{
        ADD,
        REMOVE,
        VOTING_CONSENSUS,
        VOTING_TIME,
        TAX_RATE,
        CREATOR_SHARE
    }

    event ProposalSubmitted(
        uint256 proposalId,
        address owner,
        string proposalType
    );

    event ProposalAccepted(
        uint256 proposalId
    );

    event Voted(
        uint256 proposalId,
        bool value,
        address voter
    );

    event RealmLandsMinted(
        uint256 extensionId,
        address to,
        uint256 quantity,
        uint256 tokenId
    );

    event AssetUploaded(
        uint256 assetId,
        string tokenURI,
        address fundingToken,
        uint256 startingId,
        uint256 numberOfEditions,
        uint256 mintedEditions,
        uint256 salePrice,
        uint256 rentalPrice,
        uint256 maximumRentalTime
    );

    event AssetClaimed(
        uint256 assetId,
        address fundingToken,
        uint256 tokenId,
        uint256 salePrice,
        uint256 rentalPrice,
        uint256 rentalTime
    );

    uint256 public taxRate;
    uint256 private _tokenId;

    /// @dev proposal id => proposal type
    mapping (uint256 => ProposalTypes) private _proposalTypes;

    /// @dev set values proposal id => value
    mapping (uint256 => uint256) private _setProposalValues;

    /// @dev extension ID => add proposal ID
    mapping (uint256 => uint256) public extensionToAddProposal;

    /// @dev add proposal ID => extension ID
    mapping (uint256 => uint256) public proposalToExtension;

    /// @dev remove extension proposal ID => extension ID
    mapping (uint256 => uint256) private _proposalToExtension;

    /// @dev extension ID => realmLands address
    mapping (uint256 => address) private _extensionRealmLands;

    mapping (address => mapping(uint256 => uint256)) private _deposits;

    mapping (uint256 => ProposalData) public votingProposals;

    mapping (address => mapping(uint256 => bool)) public registeredTokens;

    address private _distributionManager;

    address public protocolParameters;

    uint256 public accruedProtocolFees;
    uint256 public accruedTaxes;

    mapping (uint256 => bool) private _executed;

    address private _realmLandsFactoryAddress;

    string private _contractURI;

    uint256 private assetId;

    mapping (uint256 => Asset) public assets;

    function initialize(
        address[6] calldata addressValues,
        //address caller,
        //address upgrader,
        //address protocolParameters_,
        //address distributionManager_,
        //address nftToAttachAddress,
        //address realmLandsFactoryAddress_,
        uint256[] calldata uintValues,
        //uint256 taxRate_,
        //uint256 creatorShare,
        //uint256 votingConsensus,
        //uint256 nftToAttachId,
        uint256 votingTime_,
        string[3] memory stringValues
        //string calldata contractURI_
        //string memory name
        //string memory symbol
    ) external initializer {
        __AccessControl_init();
        __UUPSUpgradeable_init();

        _setupRole(DEFAULT_ADMIN_ROLE, addressValues[1]);
        _setupRole(REALM_OWNER, addressValues[0]);

        _realmLandsFactoryAddress = addressValues[5];

        //_setURI(uri);
        _setTaxRate(uintValues[0]);
        _setCreatorShare(uintValues[1]);
        _setCreatorAddress(addressValues[0]);
        _setVotingTime(votingTime_);
        _createExtension(addressValues[4], uintValues[3], addressValues[0]);

        _setVotingConsensus(uintValues[2]);

        _distributionManager = addressValues[3];

        protocolParameters = addressValues[2];

        _contractURI = stringValues[0];

        __ERC721_init(stringValues[1], stringValues[2]);

    }

    function updateVotingPower(uint256 tokenId) external {
        _updateVotingPower(msg.sender, tokenId);
    }

    function setValue(string memory proposalType, uint256 value_) public {

        // Queue proposal for voting
        uint256 proposalId = _queueForVoting(
            msg.sender
        );

        if (keccak256(bytes(proposalType)) == keccak256(bytes("VOTING_CONSENSUS"))) {
            // save proposal type
            _proposalTypes[proposalId] = ProposalTypes.VOTING_CONSENSUS;
        } else if (keccak256(bytes(proposalType)) == keccak256(bytes("VOTING_TIME"))) {
            // save proposal type
            _proposalTypes[proposalId] = ProposalTypes.VOTING_TIME;
        } else if (keccak256(bytes(proposalType)) == keccak256(bytes("TAX_RATE"))) {
            // save proposal type
            _proposalTypes[proposalId] = ProposalTypes.TAX_RATE;
        } else if (keccak256(bytes(proposalType)) == keccak256(bytes("CREATOR_SHARE"))) {
            // save proposal type
            _proposalTypes[proposalId] = ProposalTypes.CREATOR_SHARE;
        }

        _setProposalValues[proposalId] = value_;

        emit ProposalSubmitted(
            proposalId,
            msg.sender,
            proposalType
        );
    }


    /**
     * @notice Submit proposal to add an extension
     * @param nftToAttachAddress address of the NFT to attach
     * @param nftToAttachId ID of the NFT to attach
     */
    function addExtension(
        address nftToAttachAddress,
        uint256 nftToAttachId
    ) public payable {
        // Make sure that caller is owner of ERC721
        address tokenOwner = IERC721Metadata(nftToAttachAddress).ownerOf(nftToAttachId);
        //address approvedFor = IERC721Metadata(nftToAttachAddress).getApproved(nftToAttachId);

        require(tokenOwner == msg.sender, "You are not the owner!");
        //require(approvedFor == address(this), "Realm should be approved!");

        // Queue proposal for voting
        uint256 proposalId = _queueForVoting(
            msg.sender
        );

        votingProposals[proposalId] = ProposalData(
            nftToAttachAddress,
            nftToAttachId,
            msg.sender
        );

        // save proposal type
        _proposalTypes[proposalId] = ProposalTypes.ADD;

        uint256 depositFee = IProtocolParameters(protocolParameters).depositFee();

        if (depositFee > 0) {
            require(msg.value == depositFee, "msg.value should equal depositFee!");

            uint256 depositFeeRemaining = msg.value;

            if (taxRate > 0) {
                uint256 tax = msg.value * taxRate / 10000;
                depositFeeRemaining -= tax;
                accruedTaxes += tax;
            }

            uint256 protocolFee = IProtocolParameters(protocolParameters).protocolFee();

            if (protocolFee > 0) {
                uint256 protocolFeeValue = msg.value * protocolFee / 10000;
                depositFeeRemaining -= protocolFeeValue;
                accruedProtocolFees += protocolFeeValue;
            }

            _deposits[msg.sender][proposalId] += depositFeeRemaining;

        }

        emit ProposalSubmitted(
            proposalId,
            msg.sender,
            "ADD"
        );
    }

    function withdrawAccruedFunds() public {
        payable(_distributionManager).transfer(accruedTaxes);
        accruedTaxes = 0;

        address feeCollector = IProtocolParameters(protocolParameters).feeCollector();
        payable(feeCollector).transfer(accruedProtocolFees);
        accruedProtocolFees = 0;
    }
    
    /// @notice Submit proposal to remove an extension
    function removeExtension(
        uint256 extensionTokenId
    ) public {

        // Get add proposal ID to get token data
        uint256 addProposalId = extensionToAddProposal[extensionTokenId];
        // Get token data
        (address tokenAddress, uint256 tokenId, address owner) = _getTokenData(addProposalId);

        // Queue for voting
        uint256 proposalId = _queueForVoting(
            owner
        );

        votingProposals[proposalId] = ProposalData(
            tokenAddress,
            tokenId,
            msg.sender
        );

        // Save data in mappings
        _proposalTypes[proposalId] = ProposalTypes.REMOVE;
        _proposalToExtension[proposalId] = extensionTokenId;

        emit ProposalSubmitted(
            proposalId,
            msg.sender,
            "REMOVE"
        );
    }

    function voteForProposal(uint256 proposalId, bool voteValue) external {

        bool votingApproved = _voteForProposal(proposalId, voteValue, msg.sender);

        if (votingApproved && !_executed[proposalId]) {
            if (_proposalTypes[proposalId] == ProposalTypes.ADD) {

                (address tokenAddress, uint256 tokenId, address proposer) = _getTokenData(proposalId);

                uint256 newTokenId = _createExtension(tokenAddress, tokenId, proposer);

                extensionToAddProposal[newTokenId] = proposalId;
                proposalToExtension[proposalId] = newTokenId;

                uint256 depositValue = _deposits[msg.sender][proposalId];
                if (depositValue > 0) {
                    payable(msg.sender).transfer(depositValue);
                    _deposits[msg.sender][proposalId] = 0;
                }

            }

            if (_proposalTypes[proposalId] == ProposalTypes.REMOVE) {
                uint256 extensionId = _proposalToExtension[proposalId];

                uint256 addProposalId = extensionToAddProposal[extensionId];

                (address tokenAddress, uint256 tokenId, address owner) = _getTokenData(addProposalId);

                _unattachNFT(tokenAddress, tokenId, extensionId);

                _removeVotingPower(owner, tokenAddress, tokenId);
            }


            if (_proposalTypes[proposalId] == ProposalTypes.VOTING_CONSENSUS) {

                uint256 value_ = _setProposalValues[proposalId];

                _setVotingConsensus(value_);

            }

            if (_proposalTypes[proposalId] == ProposalTypes.VOTING_TIME) {

                uint256 value_ = _setProposalValues[proposalId];

                _setVotingTime(value_);

            }

            if (_proposalTypes[proposalId] == ProposalTypes.TAX_RATE) {

                uint256 value_ = _setProposalValues[proposalId];

                _setTaxRate(value_);

            }

            if (_proposalTypes[proposalId] == ProposalTypes.CREATOR_SHARE) {

                uint256 value_ = _setProposalValues[proposalId];

                _setCreatorShare(value_);

            }

            _executed[proposalId] = true;

            emit ProposalAccepted(
                proposalId
            );

        }

        emit Voted(proposalId, voteValue, msg.sender);
    }

    function _createExtension(
        address tokenAddress, 
        uint256 tokenId, 
        address owner
    ) internal returns (uint256 newTokenId) {
        newTokenId = _attachNFT(tokenAddress, tokenId, owner);

        _updateVotingPower(tokenAddress, tokenId);
        _createRealmLands(newTokenId);
    }

    function _createRealmLands(uint256 extensionId) internal {

        address realmLands = IRealmLandsFactory(_realmLandsFactoryAddress).newRealmLands();
        _extensionRealmLands[extensionId] = realmLands;
    }

    function uploadAsset(
        string memory tokenURI_,
        address fundingToken,
        uint256[5] memory uintValues
        // extension Id
        // number of editions
        // sale price, 
        // rental price, 
        // maximum rental time (in seconds)
    ) external {
        require(fundingToken != address(0), "Must set funding token");

        uint256 extensionId  = uintValues[0];

        require(
            _extensionRealmLands[extensionId] != address(0),
            "Extension not existent"
        );

        address tokenOwner = ownerOf(extensionId);
        
        require(tokenOwner == msg.sender, "You are not the owner of the extension");

        require(uintValues[1] > 0, "Number of editions must be > 0");
        if (uintValues[3] != 0) {
            require(uintValues[4] > 0, "Rental price is set. Must set max rental time");    
        } else {
            require(uintValues[2] > 0, "Must set sale or rental price");
        }

        uint256 startingId = IRealmLands(_extensionRealmLands[uintValues[0]]).getCurrentId();

        IRealmLands(_extensionRealmLands[uintValues[0]]).incrementId(uintValues[1]);

        assets[assetId] = Asset(
            tokenURI_,
            fundingToken,
            uintValues[0],
            startingId,
            uintValues[1],
            0,
            uintValues[2],
            uintValues[3],
            uintValues[4]
        );

        emit AssetUploaded(
            assetId,
            tokenURI_,
            fundingToken,
            startingId,
            uintValues[1],
            0,
            uintValues[2],
            uintValues[3],
            uintValues[4]
        );

        assetId += 1;
    }

    function claimAsset(
        uint256 assetId_
    ) external {
        uint256 extensionId = assets[assetId_].extensionId;
        address fundingToken = assets[assetId_].fundingToken;
        uint256 salePrice = assets[assetId_].salePrice;
        string memory uri_ = assets[assetId_].tokenURI;

        require(
            fundingToken != address(0), 
            "Asset not existent"
        );

        require(
            salePrice > 0,
            "Asset is not for sale"
        );

        require(
            assets[assetId_].mintedEditions < assets[assetId_].numberOfEditions,
            "Already minted all editions"
        );

        uint256 allowance = IERC20(fundingToken).allowance(msg.sender, address(this));
        require(allowance >= salePrice, "Allowance must be at least sale price!");

        require(
            IERC20(fundingToken).transferFrom(
                msg.sender,
                address(this),
                salePrice
            ), 
            "Error transferring ERC20 value"
        );

        uint256 tokenId_ = assets[assetId_].currentId;
        assets[assetId_].currentId += 1;
        assets[assetId_].mintedEditions += 1;

        IRealmLands(
            _extensionRealmLands[extensionId]
        ).mintWithId(
            msg.sender,
            tokenId_,
            uri_,
            0
        );

        emit AssetClaimed(
            assetId,
            fundingToken,
            tokenId_,
            salePrice,
            0,
            0
        );
    }

    function rentAsset(
        uint256 assetId_,
        uint256 rentalTime
    ) external {
        uint256 extensionId = assets[assetId_].extensionId;

        address fundingToken = assets[assetId_].fundingToken;
        uint256 rentalPrice = assets[assetId_].rentalPrice;
        uint256 maxRentalTime = assets[assetId_].maxRentalTime;
        
        string memory uri_ = assets[assetId_].tokenURI;

        require(
            fundingToken != address(0), 
            "Asset not existent"
        );

        require(
            rentalPrice > 0,
            "Asset is not for rental"
        );

        require(
            rentalTime <= maxRentalTime,
            "Rental time is more than max"
        );

        require(
            assets[assetId_].mintedEditions < assets[assetId_].numberOfEditions,
            "Already minted all editions"
        );

        uint256 price = rentalTime * rentalPrice;

        uint256 allowance = IERC20(fundingToken).allowance(msg.sender, address(this));
        require(allowance >= rentalPrice, "Allowance must be at least rental price!");

        require(
            IERC20(fundingToken).transferFrom(
                msg.sender,
                address(this),
                price
            ), 
            "Error transferring ERC20 value"
        );

        uint256 tokenId_ = assets[assetId_].currentId;
        assets[assetId_].currentId += 1;
        assets[assetId_].mintedEditions += 1;

        IRealmLands(
            _extensionRealmLands[extensionId]
        ).mintWithId(
            msg.sender,
            tokenId_,
            uri_,
            rentalTime
        );

        emit AssetClaimed(
            assetId_,
            fundingToken,
            tokenId_,
            0,
            rentalPrice,
            rentalTime
        );
    }

    function mintLand(uint256 extensionId, address to, string memory uri_) public {

        address tokenOwner = ownerOf(extensionId);
        require(tokenOwner == msg.sender, "You are not the owner of the extension");

        uint256 mintedTokenId = IRealmLands(_extensionRealmLands[extensionId]).mint(to, uri_);

        emit RealmLandsMinted(extensionId, to, 1, mintedTokenId);
    }

    function mintLandBatch(uint256 extensionId, address to, uint256 qty, string[] memory uris_) public {

        address tokenOwner = ownerOf(extensionId);
        require(tokenOwner == msg.sender, "You are not the owner of the extension");

        uint256 initialId = IRealmLands(_extensionRealmLands[extensionId]).mintBatch(to, qty, uris_);

        emit RealmLandsMinted(extensionId, to, qty, initialId);
    }


    function _attachNFT(
        address collection,
        uint256 tokenId,
        address from
    ) internal returns(uint256) {
        require(!registeredTokens[collection][tokenId], "Token already registered!");

        registeredTokens[collection][tokenId] = true;
        _tokenId += 1;
        _mint(from, _tokenId);

        return _tokenId;
    }

    function _unattachNFT(
        address collection,
        uint256 tokenId,
        uint256 extensionId
    ) internal {
        registeredTokens[collection][tokenId] = false;
        _burn(extensionId);
    }

    function _setTaxRate(uint256 taxRate_) internal {
        require(taxRate_ >= 0, "taxRate must be >= than 0");
        require(taxRate_ < 10000, "taxRate must be < than 10000");
        taxRate = taxRate_;
    }

    function getRealmLandsAddress(uint256 extensionId) public view returns (address) {
        return _extensionRealmLands[extensionId];
    }

    function supportsInterface(bytes4 interfaceId) public view virtual override(ERC721Upgradeable,AccessControlUpgradeable) returns (bool) {
        return
            interfaceId == type(IAccessControlUpgradeable).interfaceId ||
            interfaceId == type(IERC721).interfaceId ||
            interfaceId == type(IERC721Metadata).interfaceId ||
            super.supportsInterface(interfaceId);
    }

    function getDistributionManagerAddress() public view returns (address) {
        return _distributionManager;
    }

    function contractURI() public view returns (string memory) {
        return _contractURI;
    }

    function _getTokenData(uint256 proposalId) internal view returns (address tokenAddress, uint256 tokenId, address owner) {
        tokenAddress = votingProposals[proposalId].tokenAddress;
        tokenId = votingProposals[proposalId].tokenId;
        owner = votingProposals[proposalId].owner;
    }

    function getDepositFee() public view returns (uint256) {
        return IProtocolParameters(protocolParameters).depositFee();
    }
    function _authorizeUpgrade(address newImplementation) internal override onlyRole(DEFAULT_ADMIN_ROLE) {}    

}

