// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts-upgradeable/access/AccessControlUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/IERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/utils/SafeERC20Upgradeable.sol";

/**
 * @title the fee collector
 */
contract DistributionManager is UUPSUpgradeable, AccessControlUpgradeable {
    using SafeERC20Upgradeable for IERC20Upgradeable;

    bytes32 public constant UPGRADER = keccak256("UPGRADER");
    bytes32 public constant WITHDRAWER = keccak256("WITHDRAWER");

    /**
     * @dev the initializer modifier is to avoid someone initializing
     *      the implementation contract after deployment
     */
    constructor() initializer {}

    /**
     * @dev initializes the contract,
     *      sets the default (initial) values of the parameters
     *      and also transfers the ownership to the governance
     */
    function initialize(address upgrader_) public initializer {
        __AccessControl_init();
        __UUPSUpgradeable_init();

        _setupRole(DEFAULT_ADMIN_ROLE, msg.sender);
        _setupRole(UPGRADER, upgrader_);
    }

    /**
     * @notice allows to withdraw accrued funds from the contract
     */
    function withdrawTo(
        address account_,
        address token_,
        uint256 amount_
    ) external onlyRole(WITHDRAWER) {
        IERC20Upgradeable(token_).safeTransfer(account_, amount_);
    }

    /**
     * @notice allows to withdraw accrued funds from the contract
     */
    function withdrawETHTo(
        address account_,
        uint256 amount_
    ) external onlyRole(WITHDRAWER) {
        payable(account_).transfer(amount_);
    }

    // solhint-disable-next-line no-empty-blocks
    function _authorizeUpgrade(address) internal view override onlyRole(UPGRADER) {}
}