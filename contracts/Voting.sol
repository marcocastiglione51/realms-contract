// SPDX-License-Identifier: MIT

pragma solidity 0.8.4;

import "@openzeppelin/contracts/token/ERC721/IERC721.sol";

abstract contract Voting {

    uint256 private _votingProposalCounter;
    uint256 public votingPower;
    address private _creatorAddress;
    uint256 private _creatorShare;
    uint256 private _votingConsensus;
    uint256 private _votingTime;

    mapping (address => uint256) public votingPowerByAddress;

    struct Proposal {
        address owner;
        uint256 votes;
        uint256 votingEnd;
    }

    mapping(uint256 => Proposal) public proposals;
    mapping(address => mapping(uint256 => bool)) hasVotedForProposal;

    mapping(address => mapping(uint256 => address)) private _tokenOwners;

    function _updateVotingPower(
        address collection,
        uint256 tokenId
    ) internal {

        address lastOwner = _tokenOwners[collection][tokenId];

        address ownerOf = IERC721(collection).ownerOf(tokenId);

        if (lastOwner != ownerOf) {

            votingPower += 10000;

            votingPowerByAddress[_creatorAddress] += _creatorShare;
            votingPowerByAddress[ownerOf] += (10000 - _creatorShare);

            if (lastOwner != address(0)) {
                votingPowerByAddress[_creatorAddress] -= _creatorShare;
                votingPowerByAddress[lastOwner] -= (10000 - _creatorShare);
            }
        }
    }

    
    function _removeVotingPower(
        address owner, 
        address collection,
        uint256 tokenId
    ) internal {

        votingPower -= 10000;
        uint256 votingPowerOwner = 10000 - _creatorShare;

        votingPowerByAddress[_creatorAddress] -= _creatorShare;
        votingPowerByAddress[owner] -= votingPowerOwner;
        _tokenOwners[collection][tokenId] = address(0);
    }

    function _queueForVoting(
        address caller
    ) internal returns (uint256) {
        _votingProposalCounter += 1;
        
        proposals[_votingProposalCounter] = Proposal(
            caller,
            0,
            block.timestamp + _votingTime
        );

        return _votingProposalCounter;
    }

    function _voteForProposal(uint256 proposalId, bool voteValue, address caller) internal returns (bool) {
        require(
            proposals[proposalId].votingEnd > 0, 
            "Non existent proposal"
        );

        require(
            hasVotedForProposal[caller][proposalId] == false,
            "Can't vote for same proposal twice!"
        );

        require(
            proposals[proposalId].votingEnd > block.timestamp,
            "Proposal has expired"
        );

        require(
            votingPowerByAddress[caller] > 0,
            "You do not have voting power"
        );

        if (voteValue) {
            proposals[proposalId].votes += votingPowerByAddress[caller];
        }

        hasVotedForProposal[caller][proposalId] = true;

        if (proposals[proposalId].votes > votingPower * _votingConsensus / 10000) {
            return true;
        } else {
            return false;
        }
    }

    function _setCreatorAddress(
        address creatorAddress
    ) internal {
        require(creatorAddress != address(0), "creatorAddress can't be address zero");
        _creatorAddress = creatorAddress;
    }

    function _setCreatorShare(
        uint256 creatorShare
    ) internal {
        require(creatorShare > 0, "creatorShare must be > than 0");
        require(creatorShare < 10000, "creatorShare must be =< 10000");
        _creatorShare = creatorShare;
    }

    function _setVotingConsensus(
        uint256 votingConsensus
    ) internal {
        require(votingConsensus > 0, "votingConsensus must be > than 0");
        require(votingConsensus <= 10000, "votingConsensus must be =< 10000");
        _votingConsensus = votingConsensus;
    }

    function _setVotingTime(
        uint256 votingTime
    ) internal {
        require(votingTime > 0, "votingTime must be greater than zero");
        _votingTime = votingTime;
    }

}