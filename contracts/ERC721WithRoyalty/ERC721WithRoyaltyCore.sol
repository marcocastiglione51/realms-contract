// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/utils/introspection/ERC165.sol";
import "@openzeppelin/contracts/interfaces/IERC2981.sol";
import "@openzeppelin/contracts/utils/structs/EnumerableSet.sol";

import "./IERC721WithRoyalty.sol";

abstract contract ERC721WithRoyaltyCore is
    IERC2981,
    IERC721WithRoyalty,
    ERC165
{
    using EnumerableSet for EnumerableSet.UintSet;

    TokenRoyalty public defaultRoyalty;
    mapping(uint256 => TokenRoyalty) private _tokenRoyalties;
    EnumerableSet.UintSet private _tokensWithRoyalties;

    function supportsInterface(bytes4 interfaceId)
        public
        view
        virtual
        override(ERC165, IERC165)
        returns (bool)
    {
        return
            interfaceId == type(IERC2981).interfaceId ||
            interfaceId == type(IERC721WithRoyalty).interfaceId ||
            super.supportsInterface(interfaceId);
    }

    /**
     * @dev Sets token royalties. When you override this in the implementation contract
     * ensure that you access restrict it to the contract owner or admin
     */
    function _setTokenRoyalties(TokenRoyaltyConfig[] memory royaltyConfigs)
        internal
    {
        for (uint256 i = 0; i < royaltyConfigs.length; i++) {
            TokenRoyaltyConfig memory royaltyConfig = royaltyConfigs[i];
            require(royaltyConfig.bps < 10000, "Invalid bps");
            if (royaltyConfig.recipient == address(0)) {
                delete _tokenRoyalties[royaltyConfig.tokenId];
                _tokensWithRoyalties.remove(royaltyConfig.tokenId);
                emit TokenRoyaltyRemoved(royaltyConfig.tokenId);
            } else {
                _tokenRoyalties[royaltyConfig.tokenId] = TokenRoyalty(
                    royaltyConfig.recipient,
                    royaltyConfig.bps
                );
                _tokensWithRoyalties.add(royaltyConfig.tokenId);
                emit TokenRoyaltySet(
                    royaltyConfig.tokenId,
                    royaltyConfig.recipient,
                    royaltyConfig.bps
                );
            }
        }
    }

    /**
     * @dev Sets default royalty. When you override this in the implementation contract
     * ensure that you access restrict it to the contract owner or admin
     */
    function _setDefaultRoyalty(TokenRoyalty memory royalty) internal {
        require(royalty.bps < 10000, "Invalid bps");
        defaultRoyalty = TokenRoyalty(royalty.recipient, royalty.bps);
        emit DefaultRoyaltySet(royalty.recipient, royalty.bps);
    }

    /**
     * @dev See {IEIP2981RoyaltyOverride-getTokenRoyaltiesCount}.
     */
    function getTokenRoyaltiesCount() external view override returns (uint256) {
        return _tokensWithRoyalties.length();
    }

    /**
     * @dev See {IEIP2981RoyaltyOverride-getTokenRoyaltyByIndex}.
     */
    function getTokenRoyaltyByIndex(uint256 index)
        external
        view
        override
        returns (TokenRoyaltyConfig memory)
    {
        uint256 tokenId = _tokensWithRoyalties.at(index);
        TokenRoyalty memory royalty = _tokenRoyalties[tokenId];
        return TokenRoyaltyConfig(tokenId, royalty.recipient, royalty.bps);
    }

    /**
     * @dev See {IEIP2981RoyaltyOverride-royaltyInfo}.
     */
    function royaltyInfo(uint256 tokenId, uint256 value)
        public
        view
        override
        returns (address, uint256)
    {
        if (_tokenRoyalties[tokenId].recipient != address(0)) {
            return (
                _tokenRoyalties[tokenId].recipient,
                (value * _tokenRoyalties[tokenId].bps) / 10000
            );
        }
        if (defaultRoyalty.recipient != address(0) && defaultRoyalty.bps != 0) {
            return (
                defaultRoyalty.recipient,
                (value * defaultRoyalty.bps) / 10000
            );
        }
        return (address(0), 0);
    }
}
