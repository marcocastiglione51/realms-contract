// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

interface IRealm {

    function updateVotingPower(uint256 tokenId) external;

    function setValue(string memory proposalType, uint256 value_) external;

    function addExtension(
        address nftToAttachAddress,
        uint256 nftToAttachId
    ) external;

    function withdrawAccruedFunds() external;

    function removeExtension(
        uint256 extensionTokenId
    ) external;

    function voteForProposal(uint256 proposalId, bool voteValue) external;

    function mintLand(uint256 extensionId, address to) external;


    function getRealmLandsAddress(uint256 extensionId) external view returns (address);

    function getDistributionManagerAddress() external view returns (address);

    function getDepositFee() external view returns (uint256);

}
