// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "./ERC721WithRoyaltyCore.sol";
import "./IRealm.sol";

contract ERC721WithRoyalty is ERC721WithRoyaltyCore, ERC721, Ownable {

    mapping (address => bool) public whitelistedRealms;

    mapping(uint256 => address) private _tokenRealms;

    function whitelistRealm(address realm_) external onlyOwner {
        whitelistedRealms[realm_] = !whitelistedRealms[realm_];
    }

    function attachToRealm(uint256 tokenId) external override {
        require(whitelistedRealms[msg.sender], "Caller is not a whitelisted realm!");

        _tokenRealms[tokenId] = msg.sender;

        emit AttachedToRealm(tokenId, msg.sender);
    }

    mapping(uint256 => string) private tokenToUri;

    uint256 public counter;
    mapping(uint256 => string) private _proofOfAuthenticity;

    event RoyaltyNFT(
        uint256 initialId,
        uint256 amount,
        address owner,
        string uri,
        address royaltyAddress,
        uint16 bps
    );

    event BatchMinting(
        bytes32 batchId,
        uint256 startingId,
        uint256 endingId
    );

    event AttachedToRealm(
        uint256 tokenId,
        address realm
    );

    bool public internalURIEnabled;

    string private _internalURI;

    uint256 _maxMintQty;

    /// @dev batch ID => batch data
    mapping(bytes32 => BatchData) _batchData;

    struct BatchData {
        address to;
        uint256 remainingQtyToMint;
        string uri;
        address royaltyAddress;
        uint16 bps;
        string proofOfAuthenticity;
    }

    function setInternalURI(string calldata internalURI_) public onlyOwner {
        _internalURI = internalURI_;
    }

    constructor(string memory name, string memory symbol)
        ERC721(name, symbol)
    {}

    function setTokenRoyalties(TokenRoyaltyConfig[] calldata royaltyConfigs)
        external
        override
        onlyOwner
    {
        _setTokenRoyalties(royaltyConfigs);
    }

    function setDefaultRoyalty(TokenRoyalty calldata royalty)
        external
        override
        onlyOwner
    {
        _setDefaultRoyalty(royalty);
    }

    function exists(uint256 tokenId) public view returns (bool) {
        return _exists(tokenId);
    }

    function mint(address to, string calldata uri) public {
        counter = counter + 1;
        _mint(to, counter, uri);
    }

    function proofOfAuthenticity(uint256 id) public view returns (string memory) {
        return _proofOfAuthenticity[id];
    }

    function mintBatchWithRoyalty(
        address to,
        uint256 amount,
        string calldata uri,
        address royaltyAddress,
        uint16 bps,
        string calldata proofOfAuthenticity_
    ) public onlyOwner {

        uint256 initialId = counter + 1;

        for (uint256 i = 0; i < amount; i++) {
            _mintWithRoyalty(
                to,
                uri,
                royaltyAddress,
                bps,
                proofOfAuthenticity_
            );
        }

        emit RoyaltyNFT(initialId, amount, to, uri, royaltyAddress, bps);
    }

    function mintMasterBatch(
        address to,
        uint256 quantity,
        string calldata uri,
        address royaltyAddress,
        uint16 bps,
        string calldata proofOfAuthenticity_
    ) public onlyOwner returns (bytes32 batchId) {

        require(quantity > 0, "Amount can't be 0");

        uint256 initialId = counter + 1;

        batchId = keccak256(abi.encode(address(this), initialId));

        uint256 qtyToMint;

        if (quantity < _maxMintQty) {
            qtyToMint = quantity;
        } else {
            qtyToMint = _maxMintQty;
            quantity -= _maxMintQty;
            _batchData[batchId] = BatchData(
                to,
                quantity,
                uri,
                royaltyAddress,
                bps,
                proofOfAuthenticity_
            );
        }

        _mintBatchWithRoyalty(
            batchId,
            initialId,
            to,
            qtyToMint,
            uri,
            royaltyAddress,
            bps,
            proofOfAuthenticity_
        );

    }

    function mintBatchFromId(bytes32 batchId) public onlyOwner {

        address to = _batchData[batchId].to;
        uint256 remainingQtyToMint = _batchData[batchId].remainingQtyToMint;
        string memory uri = _batchData[batchId].uri;
        address royaltyAddress = _batchData[batchId].royaltyAddress;
        uint16 bps = _batchData[batchId].bps;
        string memory proofOfAuthenticity_ = _batchData[batchId].proofOfAuthenticity;

        require(remainingQtyToMint > 0, "Invalid batchId or already minted all tokens");

        uint256 initialId = counter + 1;
        uint256 qtyToMint;

        if (remainingQtyToMint < _maxMintQty) {
            qtyToMint = remainingQtyToMint;
        } else {
            qtyToMint = _maxMintQty;
            remainingQtyToMint -= _maxMintQty;
            _batchData[batchId].remainingQtyToMint = remainingQtyToMint;
        }

        _mintBatchWithRoyalty(
            batchId, 
            initialId, 
            to, 
            qtyToMint,
            uri,
            royaltyAddress,
            bps,
            proofOfAuthenticity_
        );

    }


    function _mintBatchWithRoyalty(
        bytes32 batchId,
        uint256 initialId,
        address to,
        uint256 qtyToMint,
        string memory uri,
        address royaltyAddress,
        uint16 bps,
        string memory proofOfAuthenticity_
    ) internal {
        for (uint256 i = 0; i < qtyToMint; i++) {
            _mintWithRoyalty(
                to,
                uri,
                royaltyAddress,
                bps,
                proofOfAuthenticity_
            );
        }

        emit RoyaltyNFT(initialId, qtyToMint, to, uri, royaltyAddress, bps);
        emit BatchMinting(batchId, initialId, initialId+qtyToMint);
    }

    function mintWithRoyalty(
        address to,
        string memory uri,
        address royaltyAddress,
        uint16 bps,
        string memory proofOfAuthenticity_
    ) public onlyOwner {

        uint256 tokenId = counter + 1;

        _mintWithRoyalty(
            to,
            uri,
            royaltyAddress,
            bps,
            proofOfAuthenticity_
        );

        emit RoyaltyNFT(tokenId, 1, to, uri, royaltyAddress, bps);
    }

    function _mintWithRoyalty(
        address to,
        string memory uri,
        address royaltyAddress,
        uint16 bps,
        string memory proofOfAuthenticity_
    ) internal {
        counter = counter + 1;
        _mint(to, counter, uri);

        TokenRoyaltyConfig[] memory config = new TokenRoyaltyConfig[](1);
        config[0] = TokenRoyaltyConfig(counter, royaltyAddress, bps);
        _setTokenRoyalties(config);

        _proofOfAuthenticity[counter] = proofOfAuthenticity_;
    }

    function safeMint(address to, string memory uri) public onlyOwner {
        counter = counter + 1;
        _safeMint(to, counter, uri);
    }

    function safeMint(
        address to,
        string memory uri,
        bytes memory _data
    ) public onlyOwner {
        counter = counter + 1;
        _safeMint(to, counter, uri, _data);
    }

    function burn(uint256 tokenId) public {
        _burn(tokenId);
    }

    function tokenURI(uint256 tokenId)
        public
        view
        override
        returns (string memory)
    {
        require(
            _exists(tokenId),
            "ERC721Metadata: URI query for nonexistent token"
        );

        if (internalURIEnabled) {
            return _internalURI;
        } else {
            return tokenToUri[tokenId];
        }
        
    }

    function supportsInterface(bytes4 interfaceId)
        public
        view
        virtual
        override(ERC721, ERC721WithRoyaltyCore)
        returns (bool)
    {
        return super.supportsInterface(interfaceId);
    }

    function _mint(
        address to,
        uint256 tokenId,
        string memory uri
    ) private {
        super._mint(to, tokenId);
        tokenToUri[tokenId] = uri;
    }

    function _safeMint(
        address to,
        uint256 tokenId,
        string memory uri // bytes memory _data
    ) private {
        super._safeMint(to, tokenId);
        tokenToUri[tokenId] = uri;
    }

    function _safeMint(
        address to,
        uint256 tokenId,
        string memory uri,
        bytes memory _data
    ) private {
        super._safeMint(to, tokenId, _data);
        tokenToUri[tokenId] = uri;
    }
    
    function setMaxMintQty(uint256 maxMintQty_) public onlyOwner {
        _maxMintQty = maxMintQty_;
    }


    function transferFrom(
        address from,
        address to,
        uint256 tokenId
    ) public virtual override {
        //solhint-disable-next-line max-line-length
        require(_isApprovedOrOwner(_msgSender(), tokenId), "ERC721: transfer caller is not owner nor approved");

        if (_tokenRealms[tokenId] != address(0)) {
            IRealm(_tokenRealms[tokenId]).updateVotingPower(tokenId);
        }

        _transfer(from, to, tokenId);
    }
}
