const { ethers } = require('hardhat');
const chai = require('chai');
chai.use(waffle.solidity);
const { expect } = chai;
const { getEventArgs } = require('./helpers/events');

describe('Realm', function () {


  let rc;
  beforeEach(async function () {
    ADDRESS_ZERO = '0x0000000000000000000000000000000000000000';
    [account, account2] = await ethers.getSigners();
    await deployments.fixture(['realm', 'creator', 'erc721mock', 'erc20mock', 'erc721']);
    rc = await ethers.getContract('RealmCreator');
    erc721mock = await ethers.getContract('ERC721Mock');
    erc20mock = await ethers.getContract('ERC20Mock');
    await erc721mock.mint(account.address, 0);
    await erc20mock.mint(account.address, 1000000);
  });

  it('should deploy realmCreator', async () => {
    assert.ok(rc.address);
  });

  it('should create a new realm successfully', async () => {

    const createTx = rc.createRealm([1000, 2000, 5100, 0], erc721mock.address,  '', '', '');

    await expect(createTx).to.not.be.reverted;
    await expect(createTx).to.emit(rc, 'RealmCreated');
  });

  it('Distribution manager address', async () => { 

    const createTx = await rc.createRealm([1000, 2000, 5100, 0], erc721mock.address,  '', '', '');
    await expect(createTx).to.emit(rc, 'RealmCreated');
    args = await getEventArgs(createTx, 'RealmCreated', rc);

    realmAddress = args.realmAddress;

    const realm = await ethers.getContractAt('Realm', realmAddress);

    const distributionManager = await ethers.getContract('DistributionManager');

    const distributionManagerAddress = await realm.getDistributionManagerAddress();
    expect(distributionManagerAddress).to.be.equal(distributionManager.address);

  });

  it('taxRate should be 1000', async () => {

    const createTx = await rc.createRealm([1000, 2000, 5100, 0], erc721mock.address,  '', '', '');
    await expect(createTx).to.emit(rc, 'RealmCreated');
    args = await getEventArgs(createTx, 'RealmCreated', rc);

    realmAddress = args.realmAddress;

    const realm = await ethers.getContractAt('Realm', realmAddress);

    const taxRate = await realm.taxRate();
    expect(taxRate).to.be.equal(1000);

  });

  it('Voting power should be 10000', async () => {
    const createTx = await rc.createRealm([1000, 2000, 5100, 0], erc721mock.address,  '', '', '');
    await expect(createTx).to.emit(rc, 'RealmCreated');
    args = await getEventArgs(createTx, 'RealmCreated', rc);

    realmAddress = args.realmAddress;

    const realm = await ethers.getContractAt('Realm', realmAddress);
    
    const votingPowerByAddress = await realm.votingPowerByAddress(account.address);

    expect(votingPowerByAddress).to.be.equal('10000');
  });

  it('Voting power after adding extension', async () => {

    await erc721mock.mint(account2.address, 1);

    const createTx = await rc.createRealm([1000, 2000, 5100, 0], erc721mock.address,  '', '', '');
    await expect(createTx).to.emit(rc, 'RealmCreated');
    args = await getEventArgs(createTx, 'RealmCreated', rc);

    realmAddress = args.realmAddress;

    const realm = await ethers.getContractAt('Realm', realmAddress);

    const depositFee = await realm.getDepositFee();

    const addExtensionTx = await realm.connect(account2).addExtension(erc721mock.address, 1, {value:depositFee});
    await expect(addExtensionTx).to.emit(realm, 'ProposalSubmitted');
    addArgs = await getEventArgs(addExtensionTx, 'ProposalSubmitted', realm);

    proposalId = addArgs.proposalId;

    await realm.voteForProposal(proposalId.toString(), true);

    const votingPowerOwner = await realm.votingPowerByAddress(account.address);

    expect(votingPowerOwner).to.be.equal('12000');

    const votingPowerAdded = await realm.votingPowerByAddress(account2.address);

    expect(votingPowerAdded).to.be.equal('8000');


  });

  it('Voting power after deleting extension', async () => {

    await erc721mock.mint(account2.address, 1);

    const createTx = await rc.createRealm([1000, 2000, 5100, 0], erc721mock.address,  '', '', '');
    await expect(createTx).to.emit(rc, 'RealmCreated');
    args = await getEventArgs(createTx, 'RealmCreated', rc);

    realmAddress = args.realmAddress;

    const realm = await ethers.getContractAt('Realm', realmAddress);

    const depositFee = await realm.getDepositFee();

    const addExtensionTx = await realm.connect(account2).addExtension(erc721mock.address, 1, {value:depositFee});
    await expect(addExtensionTx).to.emit(realm, 'ProposalSubmitted');
    addArgs = await getEventArgs(addExtensionTx, 'ProposalSubmitted', realm);

    proposalId = addArgs.proposalId;

    await realm.voteForProposal(proposalId, true);

    const extensionId = await realm.proposalToExtension(proposalId);

    const removeExtensionTx = await realm.removeExtension(extensionId);

    await expect(removeExtensionTx).to.emit(realm, 'ProposalSubmitted');
    removeArgs = await getEventArgs(removeExtensionTx, 'ProposalSubmitted', realm);

    remProposalId = removeArgs.proposalId;

    await realm.voteForProposal(remProposalId, true);

    const votingPowerOwner = await realm.votingPowerByAddress(account.address);

    expect(votingPowerOwner).to.be.equal('10000');

    const votingPowerAdded = await realm.votingPowerByAddress(account2.address);

    expect(votingPowerAdded).to.be.equal('0');


  });

  it('Voting power after transfer token', async () => {

    const erc721wr = await ethers.getContract('ERC721WithRoyalty');

    await erc721wr.mintWithRoyalty(
      account.address,
      "QmYraR7SJoDjeNhXMPKArST26BSBaypwoNuReWFwKvZy3q",
      "0xf2d09a2B2Db41B37C90509aB554A7aaf5e19956f",
      1200,
      ''
    );

    const createTx = await rc.createRealm([1000, 2000, 5100, 1], erc721wr.address, '', '', '');
    await expect(createTx).to.emit(rc, 'RealmCreated');
    args = await getEventArgs(createTx, 'RealmCreated', rc);

    realmAddress = args.realmAddress;

    const realm = await ethers.getContractAt('Realm', realmAddress);

    await realm.transferFrom(account.address, account2.address, 1);

    const votingPowerOwner = await realm.votingPowerByAddress(account.address);

    expect(votingPowerOwner).to.be.equal('2000');

    const votingPowerAdded = await realm.votingPowerByAddress(account2.address);

    expect(votingPowerAdded).to.be.equal('8000');


  });

  it('Mint realm lands', async () => {
    const createTx = await rc.createRealm([1000, 2000, 5100, 0], erc721mock.address,  '', '', '');
    await expect(createTx).to.emit(rc, 'RealmCreated');
    args = await getEventArgs(createTx, 'RealmCreated', rc);

    realmAddress = args.realmAddress;

    const realm = await ethers.getContractAt('Realm', realmAddress);
    
    await realm.mintLand(1, account.address, '');

    const realmLandsAddress = await realm.getRealmLandsAddress(1);

    const RealmLands = await ethers.getContractAt('RealmLands', realmLandsAddress);

    const balance = await RealmLands.balanceOf(account.address);

    expect(balance).to.be.equal(1);
  });

  it('Mint realm lands batch wrong amount of URIs', async () => {
    const createTx = await rc.createRealm([1000, 2000, 5100, 0], erc721mock.address,  '', '', '');
    await expect(createTx).to.emit(rc, 'RealmCreated');
    args = await getEventArgs(createTx, 'RealmCreated', rc);

    realmAddress = args.realmAddress;

    const realm = await ethers.getContractAt('Realm', realmAddress);
    
    const tx = realm.mintLandBatch(1, account.address, 2, ['']);

    await expect(tx).to.be.revertedWith('uris must be same length as quantity to mint');

  });

  it('Mint realm lands batch', async () => {
    const createTx = await rc.createRealm([1000, 2000, 5100, 0], erc721mock.address,  '', '', '');
    await expect(createTx).to.emit(rc, 'RealmCreated');
    args = await getEventArgs(createTx, 'RealmCreated', rc);

    realmAddress = args.realmAddress;

    const realm = await ethers.getContractAt('Realm', realmAddress);
    
    await realm.mintLandBatch(1, account.address, 2, ['', '']);

    const realmLandsAddress = await realm.getRealmLandsAddress(1);

    const RealmLands = await ethers.getContractAt('RealmLands', realmLandsAddress);

    const balance = await RealmLands.balanceOf(account.address);

    expect(balance).to.be.equal(2);
  });


  it('addExtension', async () => {

    await erc721mock.mint(account.address, 1);

    const createTx = await rc.createRealm([1000, 2000, 5100, 0], erc721mock.address, '', '', '');
    await expect(createTx).to.emit(rc, 'RealmCreated');
    args = await getEventArgs(createTx, 'RealmCreated', rc);

    realmAddress = args.realmAddress;

    const realm = await ethers.getContractAt('Realm', realmAddress);
    
    const depositFee = await realm.getDepositFee();

    const addExtensionTx = await realm.addExtension(erc721mock.address, 1, {value:depositFee});
    await expect(addExtensionTx).to.emit(realm, 'ProposalSubmitted');
    addArgs = await getEventArgs(addExtensionTx, 'ProposalSubmitted', realm);

    proposalId = addArgs.proposalId;

    const voteTx = await realm.voteForProposal(proposalId.toString(), true);
    await expect(voteTx).to.emit(realm, 'ProposalAccepted');
    voteArgs = await getEventArgs(voteTx, 'ProposalAccepted', realm);

    expect(proposalId).to.be.equal(voteArgs.proposalId);
    
  });

  describe('Assets', function () {
  
    describe('uploadAsset', function () {

      it('fundingToken is zero', async () => {

        const createTx = await rc.createRealm([1000, 2000, 5100, 0], erc721mock.address,  '', '', '');
        await expect(createTx).to.emit(rc, 'RealmCreated');
        args = await getEventArgs(createTx, 'RealmCreated', rc);

        realmAddress = args.realmAddress;

        const realm = await ethers.getContractAt('Realm', realmAddress);

        const tx = realm.uploadAsset(
          'TEST_URI',
          ADDRESS_ZERO,
          [0,0,0,0,0]
        );

        await expect(tx).to.be.revertedWith('Must set funding token');
        
      });


      it('Number of editions must be > 0', async () => {

        const createTx = await rc.createRealm([1000, 2000, 5100, 0], erc721mock.address,  '', '', '');
        await expect(createTx).to.emit(rc, 'RealmCreated');
        args = await getEventArgs(createTx, 'RealmCreated', rc);

        realmAddress = args.realmAddress;

        const realm = await ethers.getContractAt('Realm', realmAddress);

        const tx = realm.uploadAsset(
          'TEST_URI',
          erc20mock.address,
          [1,0,0,0,0]
        );

        await expect(tx).to.be.revertedWith('Number of editions must be > 0');
        
      });

      it('Extension not existent', async () => {

        const createTx = await rc.createRealm([1000, 2000, 5100, 0], erc721mock.address,  '', '', '');
        await expect(createTx).to.emit(rc, 'RealmCreated');
        args = await getEventArgs(createTx, 'RealmCreated', rc);

        realmAddress = args.realmAddress;

        const realm = await ethers.getContractAt('Realm', realmAddress);

        const tx = realm.uploadAsset(
          'TEST_URI',
          erc20mock.address,
          [0,0,0,0,0]
        );

        await expect(tx).to.be.revertedWith('Extension not existent');
        
      });


      it('You are not the owner of the extension', async () => {

        const createTx = await rc.createRealm([1000, 2000, 5100, 0], erc721mock.address,  '', '', '');
        await expect(createTx).to.emit(rc, 'RealmCreated');
        args = await getEventArgs(createTx, 'RealmCreated', rc);

        realmAddress = args.realmAddress;

        const realm = await ethers.getContractAt('Realm', realmAddress);

        const tx = realm.connect(account2).uploadAsset(
          'TEST_URI',
          erc20mock.address,
          [1,0,0,0,0]
        );

        await expect(tx).to.be.revertedWith('You are not the owner of the extension');
        
      });

      it('Must set sale or rental price', async () => {

        const createTx = await rc.createRealm([1000, 2000, 5100, 0], erc721mock.address,  '', '', '');
        await expect(createTx).to.emit(rc, 'RealmCreated');
        args = await getEventArgs(createTx, 'RealmCreated', rc);

        realmAddress = args.realmAddress;

        const realm = await ethers.getContractAt('Realm', realmAddress);

        const tx = realm.uploadAsset(
          'TEST_URI',
          erc20mock.address,
          [1,1,0,0,0]
        );

        await expect(tx).to.be.revertedWith('Must set sale or rental price');
        
      });

      it('Rental price is set. Must set max rental time', async () => {

        const createTx = await rc.createRealm([1000, 2000, 5100, 0], erc721mock.address,  '', '', '');
        await expect(createTx).to.emit(rc, 'RealmCreated');
        args = await getEventArgs(createTx, 'RealmCreated', rc);

        realmAddress = args.realmAddress;

        const realm = await ethers.getContractAt('Realm', realmAddress);

        const tx = realm.uploadAsset(
          'TEST_URI',
          erc20mock.address,
          [1,1,0,1,0]
        );

        await expect(tx).to.be.revertedWith('Rental price is set. Must set max rental time');
        
      });


      it('Ok', async () => {

        const createTx = await rc.createRealm([1000, 2000, 5100, 0], erc721mock.address,  '', '', '');
        await expect(createTx).to.emit(rc, 'RealmCreated');
        args = await getEventArgs(createTx, 'RealmCreated', rc);

        realmAddress = args.realmAddress;

        const realm = await ethers.getContractAt('Realm', realmAddress);

        await realm.uploadAsset(
          'TEST_URI',
          erc20mock.address,
          [1,1,30000,0,0]
        );

        await realm.uploadAsset(
          'TEST_URI',
          erc20mock.address,
          [1,1,0,1,36800]
        );

        
      });

      it('Verify token ID', async () => {

        const createTx = await rc.createRealm([1000, 2000, 5100, 0], erc721mock.address,  '', '', '');
        await expect(createTx).to.emit(rc, 'RealmCreated');
        args = await getEventArgs(createTx, 'RealmCreated', rc);

        realmAddress = args.realmAddress;

        const realm = await ethers.getContractAt('Realm', realmAddress);

        const realmLandsAddress = await realm.getRealmLandsAddress(1);
        const realmLands = await ethers.getContractAt('RealmLands', realmLandsAddress);

        const initialId = await realmLands.getCurrentId();;

        await realm.uploadAsset(
          'TEST_URI',
          erc20mock.address,
          [1,1,30000,0,0]
        );

        let endingId = await realmLands.getCurrentId();

        expect(endingId).to.be.equal(initialId+1);

        await realm.uploadAsset(
          'TEST_URI',
          erc20mock.address,
          [1,5,30000,0,0]
        );

        endingId = await realmLands.getCurrentId();

        expect(endingId).to.be.equal(initialId+6);
        
      });


    });

    describe('claimAsset', function () {

      it('Asset not existent', async () => {

        const createTx = await rc.createRealm([1000, 2000, 5100, 0], erc721mock.address,  '', '', '');
        await expect(createTx).to.emit(rc, 'RealmCreated');
        args = await getEventArgs(createTx, 'RealmCreated', rc);

        realmAddress = args.realmAddress;

        const realm = await ethers.getContractAt('Realm', realmAddress);

        const realmLandsAddress = await realm.getRealmLandsAddress(1);
        const realmLands = await ethers.getContractAt('RealmLands', realmLandsAddress);

        const initialId = await realmLands.getCurrentId();

        await realm.uploadAsset(
          'TEST_URI',
          erc20mock.address,
          [1,1,30000,0,0]
        );

        const tx = realm.claimAsset(
          1
        );

        await expect(tx).to.be.revertedWith('Asset not existent');

      });


      it('Allowance', async () => {

        const createTx = await rc.createRealm([1000, 2000, 5100, 0], erc721mock.address,  '', '', '');
        await expect(createTx).to.emit(rc, 'RealmCreated');
        args = await getEventArgs(createTx, 'RealmCreated', rc);

        realmAddress = args.realmAddress;

        const realm = await ethers.getContractAt('Realm', realmAddress);

        const realmLandsAddress = await realm.getRealmLandsAddress(1);
        const realmLands = await ethers.getContractAt('RealmLands', realmLandsAddress);

        await realm.uploadAsset(
          'TEST_URI',
          erc20mock.address,
          [1,1,30000,0,0]
        );

        const tx = realm.claimAsset(
          0
        );

        await expect(tx).to.be.revertedWith('Allowance must be at least sale price!');        
      });

      it("Already minted all editions. Verify token IDs", async () => {

        const createTx = await rc.createRealm([1000, 2000, 5100, 0], erc721mock.address,  '', '', '');
        await expect(createTx).to.emit(rc, 'RealmCreated');
        args = await getEventArgs(createTx, 'RealmCreated', rc);

        realmAddress = args.realmAddress;

        const realm = await ethers.getContractAt('Realm', realmAddress);

        const realmLandsAddress = await realm.getRealmLandsAddress(1);
        const realmLands = await ethers.getContractAt('RealmLands', realmLandsAddress);

        await realm.uploadAsset(
          'TEST_URI',
          erc20mock.address,
          [1,1,30000,0,0]
        );

        await erc20mock.approve(realm.address, 60000);

        await realm.claimAsset(
          0
        );

        let tx = realm.claimAsset(
          0
        );

        await expect(tx).to.be.revertedWith('Already minted all editions');


        await realm.uploadAsset(
          'TEST_URI',
          erc20mock.address,
          [1,10,30000,0,0]
        );

        await erc20mock.approve(realm.address, 300000);

        await realm.claimAsset(1);
        await realm.claimAsset(1);
        await realm.claimAsset(1);
        await realm.claimAsset(1);
        await realm.claimAsset(1);
        await realm.claimAsset(1);
        await realm.claimAsset(1);
        await realm.claimAsset(1);
        await realm.claimAsset(1);

        tc = await realm.claimAsset(1);

        await expect(tx).to.be.revertedWith('Already minted all editions');

        for (let i = 0; i < 11; i++) {
          const tokenOwner = await realmLands.ownerOf(i);
          expect(tokenOwner).to.be.equal(account.address);
        }
      });

      it("Asset is not for sale", async () => {

        const createTx = await rc.createRealm([1000, 2000, 5100, 0], erc721mock.address,  '', '', '');
        await expect(createTx).to.emit(rc, 'RealmCreated');
        args = await getEventArgs(createTx, 'RealmCreated', rc);

        realmAddress = args.realmAddress;

        const realm = await ethers.getContractAt('Realm', realmAddress);

        const realmLandsAddress = await realm.getRealmLandsAddress(1);
        const realmLands = await ethers.getContractAt('RealmLands', realmLandsAddress);

        await realm.uploadAsset(
          'TEST_URI',
          erc20mock.address,
          [1,1,0,1,1]
        );

        const tx = realm.claimAsset(
          0
        );

        await expect(tx).to.be.revertedWith('Asset is not for sale');

      });


    });

  


    describe('rentAsset', function () {

      it("Asset not existent", async () => {

        const createTx = await rc.createRealm([1000, 2000, 5100, 0], erc721mock.address,  '', '', '');
        await expect(createTx).to.emit(rc, 'RealmCreated');
        args = await getEventArgs(createTx, 'RealmCreated', rc);

        realmAddress = args.realmAddress;

        const realm = await ethers.getContractAt('Realm', realmAddress);

        const realmLandsAddress = await realm.getRealmLandsAddress(1);
        const realmLands = await ethers.getContractAt('RealmLands', realmLandsAddress);

        await realm.uploadAsset(
          'TEST_URI',
          erc20mock.address,
          [1,1,30000,0,0]
        );

        await erc20mock.approve(realm.address, 60000);

        let tx = realm.rentAsset(
          1, 
          1000
        );

        await expect(tx).to.be.revertedWith('Asset not existent');

      });

      it("Rental time is more than max", async () => {

        const createTx = await rc.createRealm([1000, 2000, 5100, 0], erc721mock.address,  '', '', '');
        await expect(createTx).to.emit(rc, 'RealmCreated');
        args = await getEventArgs(createTx, 'RealmCreated', rc);

        realmAddress = args.realmAddress;

        const realm = await ethers.getContractAt('Realm', realmAddress);

        const realmLandsAddress = await realm.getRealmLandsAddress(1);
        const realmLands = await ethers.getContractAt('RealmLands', realmLandsAddress);

        await realm.uploadAsset(
          'TEST_URI',
          erc20mock.address,
          [1,1,0,10,10]
        );

        await erc20mock.approve(realm.address, 60000);

        let tx = realm.rentAsset(
          0,
          11
        );

        await expect(tx).to.be.revertedWith('Rental time is more than max');

      });

      it("Asset is not for rental", async () => {

        const createTx = await rc.createRealm([1000, 2000, 5100, 0], erc721mock.address,  '', '', '');
        await expect(createTx).to.emit(rc, 'RealmCreated');
        args = await getEventArgs(createTx, 'RealmCreated', rc);

        realmAddress = args.realmAddress;

        const realm = await ethers.getContractAt('Realm', realmAddress);

        const realmLandsAddress = await realm.getRealmLandsAddress(1);
        const realmLands = await ethers.getContractAt('RealmLands', realmLandsAddress);

        await realm.uploadAsset(
          'TEST_URI',
          erc20mock.address,
          [1,1,1000,0,0]
        );

        await erc20mock.approve(realm.address, 60000);

        let tx = realm.rentAsset(
          0,
          11
        );

        await expect(tx).to.be.revertedWith('Asset is not for rental');

      });

      it("Already minted all editions", async () => {

        const createTx = await rc.createRealm([1000, 2000, 5100, 0], erc721mock.address,  '', '', '');
        await expect(createTx).to.emit(rc, 'RealmCreated');
        args = await getEventArgs(createTx, 'RealmCreated', rc);

        realmAddress = args.realmAddress;

        const realm = await ethers.getContractAt('Realm', realmAddress);

        const realmLandsAddress = await realm.getRealmLandsAddress(1);
        const realmLands = await ethers.getContractAt('RealmLands', realmLandsAddress);

        await realm.uploadAsset(
          'TEST_URI',
          erc20mock.address,
          [1,1,0,10,10]
        );

        await erc20mock.approve(realm.address, 100);

        await realm.rentAsset(
          0,
          10
        );

        let tx = realm.rentAsset(
          0,
          10
        );

        await expect(tx).to.be.revertedWith('Already minted all editions');

      });

      it("Allowance", async () => {

        const createTx = await rc.createRealm([1000, 2000, 5100, 0], erc721mock.address,  '', '', '');
        await expect(createTx).to.emit(rc, 'RealmCreated');
        args = await getEventArgs(createTx, 'RealmCreated', rc);

        realmAddress = args.realmAddress;

        const realm = await ethers.getContractAt('Realm', realmAddress);

        const realmLandsAddress = await realm.getRealmLandsAddress(1);
        const realmLands = await ethers.getContractAt('RealmLands', realmLandsAddress);

        await realm.uploadAsset(
          'TEST_URI',
          erc20mock.address,
          [1,1,0,10,10]
        );

        let tx = realm.rentAsset(
          0,
          10
        );

        await expect(tx).to.be.revertedWith('Allowance must be at least rental price!');

      });

      
      it("Verify values", async () => {

        const createTx = await rc.createRealm([1000, 2000, 5100, 0], erc721mock.address,  '', '', '');
        await expect(createTx).to.emit(rc, 'RealmCreated');
        args = await getEventArgs(createTx, 'RealmCreated', rc);

        realmAddress = args.realmAddress;

        const realm = await ethers.getContractAt('Realm', realmAddress);

        const realmLandsAddress = await realm.getRealmLandsAddress(1);
        const realmLands = await ethers.getContractAt('RealmLands', realmLandsAddress);

        await realm.uploadAsset(
          'TEST_URI',
          erc20mock.address,
          [1,1,0,10,10]
        );

        await erc20mock.approve(realm.address, 100);

        await realm.rentAsset(
          0,
          10
        );

        const ownerOf = await realmLands.ownerOf(0);

        await expect(ownerOf).to.be.equal(account.address);

        const isRental = await realmLands.isRental(0);

        await expect(isRental).to.be.equal(true);

        const isRentalExpired = await realmLands.isRentalExpired(0);

        await expect(isRentalExpired).to.be.equal(false);

        const URI = await realmLands.tokenURI(0);

        await expect(URI).to.be.equal('TEST_URI');


      });



      it("Advance time. Verify values", async () => {

        const createTx = await rc.createRealm([1000, 2000, 5100, 0], erc721mock.address,  '', '', '');
        await expect(createTx).to.emit(rc, 'RealmCreated');
        args = await getEventArgs(createTx, 'RealmCreated', rc);

        realmAddress = args.realmAddress;

        const realm = await ethers.getContractAt('Realm', realmAddress);

        const realmLandsAddress = await realm.getRealmLandsAddress(1);
        const realmLands = await ethers.getContractAt('RealmLands', realmLandsAddress);

        await realm.uploadAsset(
          'TEST_URI',
          erc20mock.address,
          [1,1,0,10,10]
        );

        await erc20mock.approve(realm.address, 100);

        await realm.rentAsset(
          0,
          10
        );

        await ethers.provider.send("evm_increaseTime", [1000]);
        await ethers.provider.send("evm_mine", []);     

        const isRental = await realmLands.isRental(0);

        await expect(isRental).to.be.equal(true);

        const isRentalExpired = await realmLands.isRentalExpired(0);

        await expect(isRentalExpired).to.be.equal(true);

        const URI = realmLands.tokenURI(0);

        await expect(URI).to.be.reverted;

        const tx = realmLands.transferFrom(account.address, account2.address, 0);
        await expect(tx).to.be.revertedWith('Token rental has expired!');

      });

    });

  });

});
