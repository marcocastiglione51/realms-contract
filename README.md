## The Realm

The Realm contract is a representation of a virtual world. It can be anything but for the sake of this document let's assume it's an island.

There is land in the realm. When it's created it's assigned 4 variables describing the size of the plot:

- top
- left
- bottom
- right

Using these 4 variables we can define the size of the plot and placeable tiles, where Items can be placed.

Items can be anything as of now and adding items requires voting.

### Proposals

Citizens of the island can vote on various proposals. For now we have two special cases:

- plot expansion
- item addition

However in order for any of those proposals to be submitted an extension proposal has first to be accepted.

So this is like two-step voting process:

- submit an extension proposal and when it's accepted
- submit a plot expantion/item addition proposal.

#### Extension proposal

This kind of proposal is required for other proposals to be submitted. The Realm contract has a submitExtensionProposal method
that is used for this proposal.

#### Plot expansion proposal

This kind of proposal requires parameters. Plot expansion will extend the existing plot, so the parameters must be verified whether they are in bounds.

The method used for this proposal is 

    function submitPlotExpansionProposal(
        uint256 votingPeriod,
        uint256 extensionProposalId,
        uint256[] memory dims
    )

The extensionProposalId is the id of extension proposal that has to be accepted/executed. This needs to be refined, but the idea is
that if there is a valid extension proposal then this plot expansion proposal can be submitted.

The dims array defines 4 parameters for left, top, right, bottom configuration of the extended plot.

#### Item Proposal

Another type of proposal is the item addition proposal.

    function submitItemProposal(
        uint256 votingPeriod,
        uint256 extensionProposalId,
        uint256 x,
        uint256 y
    )

This method will probably require more parameters but for now it only accepts x and y coordinates of the plot
where the item is to be added.

### Voting

We have a Voting contract that implements the voting process.

There are three main methods:

- function \_submitProposal(uint256 votingPeriod, ProposalType proposalType, bytes memory data)
- function voteForProposal(uint256 proposalId)
- function executeProposal(uint256 proposalId)

The ProposalType struct defines the type of the proposal.

The data parameter defines proposal-related data that is bytes encoded value.

Everyone (non-citizens) can submit proposals and if their proposal gets accepted, they get citizen rights, meaning they get Voting Power.

The voting process takes place in a specified time period. Once this period ends and if there is not enough votes cast for the proposal
it changes it's status to Rejected. If during voting process the amount of votes reaches over 50% the proposal changes status to Accepted.

In the end it's necessary to execute the proposal. If it has status Accepted then a handle method is called and the status of proposal is changed to Executed.

#### Voting power

The Voting Power is the total amount of votes in the realm.

Initially only the creator of the realm has voting power of 1.

Once a new citizen is added they also get voting power. However, the realm creator can define how much of the voting power they will keep to themselves.
This is done via the creatorShare parameter.

Once the proposal gets executed, if the submitter of the proposal is not a citizen, voting power is added to their address relative to the creatorShare parameter.

To keep the creatorShare in sync, the following formula is used

    creator voting power = creator voting power + creatorShare
    new citizen voting power = 1 - creatorShare

This way the creator will always keep their voting power in the specified bounds.

### Execution

Execution of the proposals is implemented in the Realm contract. The method received the type of the proposal
and the data associated with it. This is require because the data that is used for proposal execution resides in the Realm contract.

## Realm Creator

The RealmCreator contract is used to instantiate new Realms.

It does so by creating a new Beacon proxy and calling initialization method of the Realm contract.

#### Notes

In case Realm creation is not available for everybody, it might be necessary to add AccessControl or another form of execution restriction.
